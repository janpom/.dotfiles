# Command-line fuzzy finder (fzf) custom setup

# Exact match + preview window (handy for long lines)
export FZF_DEFAULT_OPTS="--exact --preview 'echo {}' --preview-window down:3"

# More useful ctr+t
export FZF_COMPLETION_TRIGGER=''
bindkey '^T' fzf-completion
bindkey '^I' $fzf_default_completion
