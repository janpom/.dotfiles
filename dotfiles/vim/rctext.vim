set wrap
set nonumber
set tabstop=8
set textwidth=80
set showbreak=>
set spell
map <Up> g<Up>
map <Down> g<Down>
imap <Up> <Esc>g<Up>a
imap <Down> <Esc>g<Down>a
