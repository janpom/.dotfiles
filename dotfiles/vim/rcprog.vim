set nowrap
set number
set tabstop=4
set shiftwidth=4
set expandtab
set textwidth=0
set showbreak=
map <Up> <Up>
map <Down> <Down>
imap <Up> <Up>
imap <Down> <Down>
