# .dotfiles #

Dot files for quickly setting up the environment on a new machine. Dotbot (https://github.com/anishathalye/dotbot) is used for installing and managing the dot files. This setup also makes it easy to keep dot file updates in sync across all environments.

### Installation ###


```
#!bash
cd ~
git clone https://bitbucket.org/janpom/.dotfiles.git
cd .dotfiles/
./install
```
